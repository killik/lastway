<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'MessageController@index')->name('api.minimal.v1.message.index');
Route::get('{message}', 'MessageController@show')->name('api.minimal.v1.message.show');

Route::post('/', 'MessageController@store')->name('api.minimal.v1.message.store');

Route::delete('{message}', 'MessageController@destroy')->name('api.minimal.v1.message.destroy');
