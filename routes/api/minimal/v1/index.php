<?php

use Illuminate\Support\Facades\Route;

Route::namespace('V1')->group(function(){
    Route::prefix('message')->group(base_path('routes/api/minimal/v1/message.php'));
});
