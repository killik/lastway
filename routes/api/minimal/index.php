<?php

use Illuminate\Support\Facades\Route;

Route::namespace('API\\Minimal')->group(function(){
    Route::prefix('v1')->group(base_path('routes/api/minimal/v1/index.php'));
});
