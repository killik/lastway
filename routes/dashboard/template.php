<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'TemplateController@index')->name('dashboard.template.index');
Route::get('create', 'TemplateController@create')->name('dashboard.template.create');
Route::get('{template}', 'TemplateController@show')->middleware('can:view,template')->name('dashboard.template.show');
Route::get('{template}/edit', 'TemplateController@edit')->middleware('can:update,template')->name('dashboard.template.edit');

Route::post('/', 'TemplateController@store')->name('dashboard.template.store');
Route::post('{template}', 'TemplateController@update')->name('dashboard.template.update');

Route::delete('{template}', 'TemplateController@destroy')->middleware('can:delete,template')->name('dashboard.template.destroy');
