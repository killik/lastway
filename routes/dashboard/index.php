<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group(['namespace' => 'Dashboard'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard.home');
    Route::post('token', 'HomeController@token')->name('dashboard.home.token');

    Route::prefix('contact')->group(base_path('routes/dashboard/contact.php'));
    Route::prefix('group')->group(base_path('routes/dashboard/group.php'));
    Route::prefix('template')->group(base_path('routes/dashboard/template.php'));
    Route::prefix('message')->group(base_path('routes/dashboard/message.php'));
});
