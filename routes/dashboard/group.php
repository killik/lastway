<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Group')->group(function () {
    Route::get('/', 'MainController@index')->name('dashboard.group.index');
    Route::get('create', 'MainController@create')->name('dashboard.group.create');
    Route::get('{group}', 'MainController@show')->middleware('can:view,group')->name('dashboard.group.show');
    Route::get('{group}/edit', 'MainController@edit')->middleware('can:update,group')->name('dashboard.group.edit');

    Route::post('/', 'MainController@store')->name('dashboard.group.store');

    Route::patch('{group}', 'MainController@update')->name('dashboard.group.update');

    Route::delete('{group}', 'MainController@destroy')->middleware('can:delete,group')->name('dashboard.group.destroy');

    Route::prefix('{group}/member')->group(function () {
        Route::get('/', 'MemberController@index')->middleware('can:view,group')->name('dashboard.group.member.index');

        Route::middleware('can:update,group')->group(function () {
            Route::get('add', 'MemberController@create')->name('dashboard.group.member.add');
            Route::post('store', 'MemberController@store')->name('dashboard.group.member.store');
            Route::delete('{contact}', 'MemberController@destroy')->name('dashboard.group.member.destroy');
        });
    });
});
