<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Contact')->group(function () {
    Route::get('/', 'MainController@index')->name('dashboard.contact.index');
    Route::get('create', 'MainController@create')->name('dashboard.contact.create');
    Route::get('{contact}', 'MainController@show')->middleware('can:view,contact')->name('dashboard.contact.show');
    Route::get('{contact}/edit', 'MainController@edit')->middleware('can:update,contact')->name('dashboard.contact.edit');

    Route::post('/', 'MainController@store')->name('dashboard.contact.store');

    Route::patch('{contact}', 'MainController@update')->name('dashboard.contact.update');

    Route::delete('{contact}', 'MainController@destroy')->middleware('can:delete,contact')->name('dashboard.contact.destroy');

    Route::prefix('{contact}/group')->group(function () {
        Route::get('/', 'GroupController@index')->middleware('can:view,contact')->name('dashboard.contact.group.index');

        Route::middleware('can:update,contact')->group(function () {
            Route::get('add', 'GroupController@create')->name('dashboard.contact.group.add');
            Route::post('store', 'GroupController@store')->name('dashboard.contact.group.store');
            Route::delete('{group}', 'GroupController@destroy')->name('dashboard.contact.group.destroy');
        });
    });
});
