<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'MessageController@index')->name('dashboard.message.index');

Route::group(['prefix' => 'compose'], function () {
    Route::get('group/{group?}', 'MessageController@groupCompose')->name('dashboard.message.compose.group');
    Route::get('template/{template}', 'MessageController@templateCompose')->name('dashboard.message.compose.template');
    Route::get('{destination?}', 'MessageController@compose')->name('dashboard.message.compose');
});

Route::group(['prefix' => 'send'], function () {
    Route::post('/', 'MessageController@send')->name('dashboard.message.send');
    Route::post('group', 'MessageController@groupSend')->name('dashboard.message.send.group');
});

Route::middleware('can:view,message')->group(function(){
    Route::get('{message}', 'MessageController@show')->name('dashboard.message.show');
});

Route::middleware('can:delete,message')->group(function(){
    Route::delete('{message}', 'MessageController@destroy')->name('dashboard.message.destroy');

});
