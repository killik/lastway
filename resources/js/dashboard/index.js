module.exports = {
    contact: require('./contact'),
    template: require('./template'),
    message: require('./message'),
    group: require('./group')
}
