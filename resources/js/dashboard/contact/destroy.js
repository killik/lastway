module.exports = function (id) {
   const form = document.getElementById('destroyContact-' + id)

    if (window.confirm('Are you shure want to delete ' + form.dataset.phone + ' (' + form.dataset.name + ') from contacts?')) {
        form.submit()
    }
}
