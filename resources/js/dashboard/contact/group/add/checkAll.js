module.exports = function(id)
{
    checkbox1 = document.getElementById('addGroupCheckAll-'+id)
    checkbox2 = document.getElementsByName('groups[]')

    checkbox2.forEach(element => {
        element.checked = checkbox1.checked

        element.onclick = () => {
            if(!element.checked)
            {
                checkbox1.checked = element.checked
            }
        }
    });
}
