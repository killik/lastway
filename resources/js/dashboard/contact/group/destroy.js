module.exports = function(id){
    const form = document.getElementById('destroyGroup-'+id)

    if (confirm('Are you shure want to remove group '+ form.dataset.name +' from this contact?'))
    {
        form.submit()
    }
}
