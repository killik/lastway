module.exports = function (id) {
    const form = document.getElementById('destroyTemplate-' + id)

    if (window.confirm('Are you shure want to delete ' + form.dataset.name + ' from message templates?')) {
        form.submit()
    }
}
