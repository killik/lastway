module.exports = function(id){
    const form = document.getElementById('destroyMember-'+id)

    if (confirm('Are you shure want to remove contact '+ form.dataset.phone +' (' + form.dataset.name +') from this group?'))
    {
        form.submit()
    }
}
