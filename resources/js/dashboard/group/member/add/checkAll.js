module.exports = function(id)
{
    checkbox1 = document.getElementById('addMemberCheckAll-'+id)
    checkbox2 = document.getElementsByName('members[]')

    checkbox2.forEach(element => {
        element.checked = checkbox1.checked

        element.onclick = () => {
            if(!element.checked)
            {
                checkbox1.checked = element.checked
            }
        }
    });
}
