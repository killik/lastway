@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Contact
                    <form data-phone="{{ $contact->phone }}" data-name="{{ $contact->name }}" id="destroyContact-{{ $contact->id }}" action="{{ route('dashboard.contact.destroy', $contact->id) }}" method="POST" class="float-right">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="button" class="btn btn-danger btn-sm" title="Delete">
                            <i class="fa fa-trash" onclick="Dashboard.contact.destroy({{ $contact->id }})"></i>
                        </button>
                        <a class="btn btn-warning btn-sm" title="Edit" href="{{ route('dashboard.contact.edit', $contact->id) }}">
                            <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a class="btn btn-primary btn-sm" title="Send Message" href="{{ route('dashboard.message.compose.contact', $contact->id) }}">
                            <i class="fa fa-envelope"></i>
                        </a>
                        <a class="btn btn-secondary btn-sm" title="List Contacts" href="{{ route('dashboard.contact.index') }}">
                            <i class="fa fa-list"></i>
                        </a>
                    </form>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @error ('contact')
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <div class="alert alert-error" role="alert">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                    @enderror
                <div class="table-responsive">
                    <table class="table table-hover table-borderless">
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{{ $contact->name }}</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>{{ $contact->phone }}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('dashboard.contact.group.index', $contact->id) }}">
                                        Groups
                                    </a>
                                </td>
                                <td>
                                    @forelse ($contact->groups as $key => $item)
                                        @if ($key > 0)
                                            ,
                                        @endif
                                        <a href="{{ route('dashboard.group.show', $item->id) }}">
                                            {{ $item->name }}
                                        </a>
                                    @empty
                                        -
                                    @endforelse
                                </td>
                            </tr>
                            <tr>
                                <td>Created At</td>
                                <td>{{ $contact->created_at }}</td>
                            </tr>
                            <tr>
                                <td>Updated At</td>
                                <td>{{ $contact->updated_at }}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>{{ $contact->address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
