@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    List Groups
                    <div class="float-right">
                        <button onclick="Dashboard.contact.group.add.send({{ $contact->id }})" class="btn btn-primary btn-sm" title="Save">
                            <i class="fa fa-save"></i>
                        </button>
                        <a class="btn btn-danger btn-sm" title="Cancel" href="{{ route('dashboard.contact.group.index', $contact->id) }}" title="Cancel">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @error ('groups')
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                    @enderror
                <div class="table-responsive">
                    <table class="table table-hover table-borderless text-nowrap">
                        <thead>
                            <tr>
                                <td>
                                    <form id="addGroup-{{ $contact->id }}" action="{{ route('dashboard.contact.group.store', $contact->id) }}" method="POST">
                                        @csrf
                                        <input id="addGroupCheckAll-{{ $contact->id }}" type="checkbox" onclick="Dashboard.contact.group.add.checkAll({{ $contact->id }})"/>
                                    </form>
                                </td>
                                <td>#</td>
                                <td>Name</td>
                                <td>Member</td>
                                <td>Created At</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($groups as $row)
                            <tr>
                                 <td>
                                    <input type="checkbox" name="groups[]" value="{{ $row->id }}" form="addGroup-{{ $contact->id }}"/>
                                </td>
                                <td>{{ $no++ }}</td>
                                <td>{{ $row->name }}</td>
                                <td>
                                    <a href="{{ route('dashboard.group.member.index', $row->id) }}">
                                        {{ $row->members->count() }}
                                    </a>
                                </td>
                                <td>{{ $row->created_at }}</td>
                                <td>
                                    <form data-name="{{ $row->name }}" id="destroyGroup-{{ $row->id }}" action="{{ route('dashboard.group.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button type="button" onclick="Dashboard.group.destroy({{ $row->id }})" class="btn btn-danger btn-sm" title="Delete">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        <a class="btn btn-warning btn-sm" title="Edit" href="{{ route('dashboard.group.edit', $row->id) }}">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-primary btn-sm" title="Send Message" href="#" onclick="alert('Coming Soon!')">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                        <a class="btn btn-secondary btn-sm" title="View" href="{{ route('dashboard.group.show', $row->id) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">Empty</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {!! $groups->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
