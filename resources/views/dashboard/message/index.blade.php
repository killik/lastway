@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Message List
                    <a href="{{ route('dashboard.message.compose') }}" class="btn btn-primary btn-sm float-right">
                        <i class="fa fa-edit"></i> Compose
                    </a>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                        <div class="table-responsive">
                        <table class="table table-hover table-borderless text-nowrap">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Status</td>
                                    <td>Created At</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @forelse ($messages as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td><small class="badge badge-warning">{{ $row->status }}</small></td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>
                                        <a class="btn btn-secondary btn-sm" title="View" href="{{ route('dashboard.message.show', $row->id) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5" class="text-center">Empty</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    {!! $messages->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
