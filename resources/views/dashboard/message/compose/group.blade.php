@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Send Group Message
                    <div class="float-right">
                        @isset($group)
                            <a class="btn btn-danger btn-sm" title="Cancel" href="{{ route('dashboard.group.show', $group->id) }}"><i class="fa fa-times"></i></a>
                        @endisset
                        <a class="btn btn-warning btn-sm" title="Group List" href="{{ route('dashboard.group.index') }}"><i class="fa fa-list"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('dashboard.message.send.group') }}">
                        @if (session('success'))
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <div class="alert alert-success" role="alert">
                                        {{ session('success') }}
                                    </div>
                                </div>
                            </div>
                        @endif

                        @csrf

                        <div class="form-group row">
                            <label for="group" class="col-md-4 col-form-label text-md-right">Group</label>

                            <div class="col-md-6">
                                <select id="groups" class="form-control @error('group') is-invalid @enderror" name="group" required>
                                    <option value>Select Group</option>
                                    @foreach ($groups as $item)
                                        <option value="{{ $item->id }}" @if($item->id == @$group->id) selected @endif>
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('group')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-4 col-form-label text-md-right">Message</label>

                            <div class="col-md-6">
                                <textarea id="message" rows="6" class="form-control @error('message') is-invalid @enderror" name="message" required autocomplete="message" autofocus>{{ old('message') }}</textarea>

                                @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Send</button>
                                <a href="{{ route('dashboard.message.index') }}" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
