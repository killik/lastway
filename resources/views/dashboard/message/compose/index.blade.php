@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Compose Message
                    <div class="float-right">
                        <a class="btn btn-danger btn-sm" title="Contact List" href="{{ route('dashboard.contact.index') }}">
                            <i class="fa fa-user-alt"></i>
                        </a>
                        <a class="btn btn-warning btn-sm" title="Message List" href="{{ route('dashboard.message.index') }}">
                            <i class="fa fa-list"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('dashboard.message.send') }}">
                        @if (session('success'))
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <div class="alert alert-success" role="alert">
                                        {{ session('success') }}
                                    </div>
                                </div>
                            </div>
                        @endif

                        @csrf

                        <div class="form-group row">
                            <label for="destination" class="col-md-4 col-form-label text-md-right">Destination</label>

                            <div class="col-md-6">
                                <datalist id="contacts">
                                    @foreach ($contacts as $item)
                                        <option value="{{ $item->phone }}">
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </datalist>
                                <input id="destination" list="contacts" type="text" class="form-control @error('destination') is-invalid @enderror" name="destination" value="{{ old('destination') ?? $destination }}" required autocomplete="destination" autofocus>

                                @error('destination')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="destination" class="col-md-4 col-form-label text-md-right">Message</label>

                            <div class="col-md-6">
                                <select id="templates" onchange="Dashboard.message.compose.template('message')" class="form-control @error('message') is-invalid @enderror" >
                                    <option selected value="">
                                        Chose Message Templates
                                    </option>
                                    @foreach ($templates as $item)
                                        <option value="{{ $item->message }}">
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <textarea id="message" rows="6" class="form-control @error('message') is-invalid @enderror" name="message" required autocomplete="message" autofocus>{{ old('message') ?? $template }}</textarea>

                                @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
