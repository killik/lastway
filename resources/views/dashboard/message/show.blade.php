@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Message
                    <form id="destroyMessage-{{ $message->id }}" action="{{ route('dashboard.message.destroy', $message->id) }}" method="POST" class="float-right">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="button" onclick="Dashboard.message.destroy({{ $message->id }})" class="btn btn-danger btn-sm" title="Delete">
                            <i class="fa fa-trash"></i>
                        </button>
                        <a class="btn btn-secondary btn-sm" title="List Message" href="{{ route('dashboard.message.index') }}">
                            <i class="fa fa-list"></i>
                        </a>
                    </form>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @error ('contact')
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <div class="alert alert-error" role="alert">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                    @enderror
                <div class="table-responsive">
                    <table class="table table-hover table-borderless">
                        <tbody>
                            <tr>
                                <td>Destination</td>
                                <td>{{ $message->destination }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    <small class="badge badge-warning">{{ $message->status }}</small>
                                </td>
                            </tr>
                            <tr>
                                <td>Text</td>
                                <td>{{ $message->text }}</td>
                            </tr>
                            <tr>
                                <td>Created At</td>
                                <td>{{ $message->created_at }}</td>
                            </tr>
                            <tr>
                                <td>Updated At</td>
                                <td>{{ $message->updated_at }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
