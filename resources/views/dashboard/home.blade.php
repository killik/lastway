@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                SMS Gateway
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="mb-2">
                                <h6 class="font-weight-bold">Bearer Token</h6>
                                <hr/>
                                <code>
                                    {{ $user->api_token }}
                                </code>
                                <hr/>
                            </div>
                            <div class="mb-2">
                                <h6 class="font-weight-bold">
                                    Quota
                                    <span class="float-right m-0">
                                        <small>Remaining: {{ $user->quota }}x</small>
                                    </span>
                                </h6>
                                <form method="POST" action="{{ route('dashboard.home.token') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <div class="col-md-6 offset-md-3">
                                            <input placeholder="Token" id="token" type="text" class="form-control @error('token') is-invalid @enderror" name="token" required>
                                                @error('token')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-3">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Send') }}
                                            </button>
                                            @if (session('success'))
                                                <span class="btn btn-success">
                                                    {{ session('success') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                {{ __('Message') }}
                              </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <a class="btn btn-primary mb-1" href="{{ route('dashboard.message.index') }}">Menage</a>
                                <a class="btn btn-primary mb-1" href="{{ route('dashboard.template.index') }}">Template</a>
                                <a class="btn btn-primary mb-1" href="{{ route('dashboard.message.compose') }}">Compose</a>
                                <a class="btn btn-primary mb-1" href="{{ route('dashboard.message.compose.group') }}">Group Compose</a>
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Contacts & Groups
                              </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <a class="btn btn-primary mb-1" href="{{ route('dashboard.group.index') }}">Groups</a>
                                <a class="btn btn-primary mb-1" href="{{ route('dashboard.contact.index') }}">Contacts</a>
                                <a class="btn btn-primary mb-1" href="#" onclick="alert('Coming Soon!')">Import</a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    About
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Rerum modi accusantium corrupti illo repellat sed harum repellendus enim. Dolorum aut dolorem maxime quaerat atque. Deleniti temporibus a quia qui inventore modi laudantium.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
