@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    List Groups
                    <a href="{{ route('dashboard.group.create') }}" class="btn btn-primary btn-sm float-right">
                        <i class="fa fa-edit"></i> Create
                    </a>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @error ('group')
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <div class="alert alert-error" role="alert">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                    @enderror
                <div class="table-responsive">
                    <table class="table table-hover table-borderless text-nowrap">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Name</td>
                                <td>Member</td>
                                <td>Created At</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($groups as $row)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $row->name }}</td>
                                <td>
                                    <a href="{{ route('dashboard.group.member.index', $row->id) }}">
                                        {{ $row->members->count() }} Contacts
                                    </a>
                                </td>
                                <td>{{ $row->created_at }}</td>
                                <td>
                                    <form data-name="{{ $row->name }}" id="destroyGroup-{{ $row->id }}" action="{{ route('dashboard.group.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button type="button" onclick="Dashboard.group.destroy({{ $row->id }})" class="btn btn-danger btn-sm" title="Delete">
                                            <i class="fa fa-trash"></i>
                                        </button>

                                        <a class="btn btn-warning btn-sm" title="Edit" href="{{ route('dashboard.group.edit', $row->id) }}">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-primary btn-sm" title="Send Message" href="{{ route('dashboard.message.compose.group', $row->id) }}">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                        <a class="btn btn-secondary btn-sm" title="View" href="{{ route('dashboard.group.show', $row->id) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">Empty</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {!! $groups->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
