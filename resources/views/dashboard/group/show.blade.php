@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Group
                    <form data-name="{{ $group->name }}" action="{{ route('dashboard.group.destroy', $group->id) }}" method="POST" class="float-right">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="button" class="btn btn-danger btn-sm" onclick="Dashboard.group.destroy({{ $group->id }})" title="Delete"><i class="fa fa-trash"></i></button>
                        <a class="btn btn-warning btn-sm" title="Edit" href="{{ route('dashboard.group.edit', $group->id) }}"><i class="fa fa-pencil-alt"></i></a>
                        <a class="btn btn-primary btn-sm" title="Send Message" href="{{ route('dashboard.message.compose.group', $group->id) }}"><i class="fa fa-envelope"></i></a>
                        <a class="btn btn-secondary btn-sm" title="List Groups" href="{{ route('dashboard.group.index') }}"><i class="fa fa-list"></i></a>
                    </form>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @error ('contact')
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <div class="alert alert-error" role="alert">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                    @enderror
                <div class="table-responsive">
                    <table class="table table-hover table-borderless">
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{{ $group->name }}</td>
                            </tr>
                            <tr>
                                <td>Members</td>
                                <td>
                                    <a href="{{ route('dashboard.group.member.index', $group->id) }}">{{ $group->members->count() }} Contacts</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Created At</td>
                                <td>{{ $group->created_at }}</td>
                            </tr>
                            <tr>
                                <td>Updated At</td>
                                <td>{{ $group->updated_at }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
