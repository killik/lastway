@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ $group->name }}
                    <div class="float-right">
                        <a href="{{ route('dashboard.group.member.add', $group->id) }}" class="btn btn-primary btn-sm" title="Add Contacts">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-warning btn-sm" title="Show Group" href="{{ route('dashboard.group.show', $group->id) }}">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-secondary btn-sm" title="List Groups" href="{{ route('dashboard.group.index') }}">
                            <i class="fa fa-list"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @error ('contact')
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <div class="alert alert-error" role="alert">
                                {{ $message }}
                            </div>
                        </div>
                    </div>
                    @enderror
                <div class="table-responsive">
                    <table class="table table-hover table-borderless text-nowrap">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Name</td>
                                <td>Phone</td>
                                <td>Created At</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($members as $row)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->phone }}</td>
                                <td>{{ $row->created_at }}</td>
                                <td>
                                    <form data-phone="{{ $row->phone }}" data-name="{{ $row->name }}" id="destroyMember-{{ $row->id }}" action="{{ route('dashboard.group.member.destroy', [$group->id, $row->id]) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button type="button" onclick="Dashboard.group.member.destroy({{ $row->id }})" class="btn btn-danger btn-sm" title="Delete">
                                            <i class="fa fa-sign-out-alt"></i>
                                        </button>

                                        <a class="btn btn-warning btn-sm" title="Edit" href="{{ route('dashboard.contact.edit', $row->id) }}">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                        <a class="btn btn-primary btn-sm" title="Send Message" href="{{ route('dashboard.message.compose.contact', $row->id) }}">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                        <a class="btn btn-secondary btn-sm" title="View" href="{{ route('dashboard.contact.show', $row->id) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {!! $members->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
