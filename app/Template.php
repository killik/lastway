<?php

namespace App;

use App\Traits\Model\Relations\Owner;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use Owner;

    protected $fillable = [
        'name', 'message', 'user_id',
    ];
}
