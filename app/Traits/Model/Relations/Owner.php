<?php

namespace App\Traits\Model\Relations;

use App\User;

trait Owner
{
    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
