<?php

namespace App\Jobs\SMS;

use App\Facades\SMS;
use App\Message;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Message $message = null)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (isset($this->message)) {
            return $this->sendMessage($this->message);
        }

        return $this->fromDb();
    }

    protected function fromDb()
    {
        $messages = Message::whereStatus('failed_send_job')->get()->all();

        foreach ($messages as $message) {
            $this->sendMessage($message);
        }
    }

    protected function sendMessage(Message $message)
    {
        try {
            $message->update(['status' => 'queued_send_job']);
            $gateway = SMS::sendMessage($message->destination, $message->text);
            $message->update([
                'remote_id' => $gateway->getId(),
                'status' => $gateway->getStatus()
            ]);
        } catch (Exception $e) {
            Log::error(sprintf('message_id: %s, error: %s', $message->id, $e->getMessage()));
            $message->update(['status' => 'failed_send_job']);
        }
    }
}
