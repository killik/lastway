<?php

namespace App\Jobs\SMS;

use App\Facades\SMS;
use App\Message;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class UpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Message $message = null)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (isset($this->message)) {
            return $this->refreshMessage($this->message);
        }

        return $this->fromDb();
    }

    protected function fromDb()
    {
        $messages = Message::where(function($query) {
            $filter = [
                ['status', '!=', 'sent'],
                ['status', '!=', 'failed']
            ];

            $query->whereNotNull('remote_id');
            $query->where($filter);
        });

        foreach($messages->get()->all() as $message)
        {
            $this->refreshMessage($message);
        }
    }

    protected function refreshMessage(Message $message)
    {
        try {
            $gateway = SMS::getMessage($message->remote_id);
            $message->update(['status' => $gateway->getStatus()]);
        } catch (Exception $e) {
            Log::error(sprintf('message_id: %s, error: %s', $message->id, $e->getMessage()));
        }
    }
}
