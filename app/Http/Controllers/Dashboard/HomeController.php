<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\TokenRequest;
use App\Voucher;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = $request->user();

        return view('dashboard.home', compact('user'));
    }

    public function token(TokenRequest $request)
    {
        $voucher = Voucher::where('token', $request->token)->first();

        $user = $request->user();

        $user->quota = $user->quota + $voucher->quota;

        $voucher->delete();

        $user->update();

        return redirect(route('dashboard.home'))->with('success', 'Successfull!');
    }
}
