<?php

namespace App\Http\Controllers\Dashboard\Contact;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Contact\StoreRequest;
use App\Http\Requests\Dashboard\Contact\UpdateRequest;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = $request->user()->contacts()->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.contact.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $contact = $request->all();

        $contact['user_id'] = $request->user()->id;

        $contact = Contact::create($contact);

        $message = sprintf('Contact %s (%s) successfully created', $contact->phone, $contact->name);

        return redirect(route('dashboard.contact.index'))->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact, Request $request)
    {
        return view('dashboard.contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return view('dashboard.contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Contact $contact)
    {
        $message = sprintf('Contact %s (%s) successfully updated', $contact->phone, $contact->name);

        $contact->update($request->all());

        return redirect(route('dashboard.contact.show', $contact->id))->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $message = sprintf('Contact %s (%s) successfully deleted', $contact->phone, $contact->name);

        $contact->delete();

        return redirect(route('dashboard.contact.index'))->with('success', $message);
    }
}
