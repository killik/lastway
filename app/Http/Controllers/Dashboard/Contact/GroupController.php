<?php

namespace App\Http\Controllers\Dashboard\Contact;

use App\Contact;
use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Contact\Group\StoreRequest;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Contact $contact)
    {
        $groups = $contact->groups()->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.contact.group.index', compact('contact', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Contact $contact, Request $request)
    {
        $groups = $request->user()->groups()->whereDoesntHave('members', function ($query) use($contact) {
            $query->whereContactId($contact->id);
        });

        $groups = $groups->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.contact.group.add', compact('contact', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Contact $contact)
    {
        if(count($request->groups) > 1)
        {
            $message = sprintf('Some groups have been successfully added to contact %s (%s)', $contact->phone, $contact->name);
        }

        else
        {
            $message = sprintf('Group successfully added to contact %s (%s)', $contact->phone, $contact->name);
        }

        $contact->groups()->attach($request->groups);

        return redirect(route('dashboard.contact.group.index', $contact->id))->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact, Group $group)
    {
        $contact->groups()->detach($group->id);

        $message = sprintf('Group %s successfully removed from contact %s (%s)', $group->name, $contact->phone, $contact->name);

        return redirect(route('dashboard.contact.group.index', $contact->id))->with('success', $message);
    }
}
