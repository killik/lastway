<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Template\StoreRequest;
use App\Http\Requests\Dashboard\Template\UpdateRequest;
use App\Template;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $templates = $request->user()->templates()->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.template.index', compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->only('name', 'message');

        $message = sprintf('Template %s successfully stored', $request->name);

        $data['user_id'] = $request->user()->id;

        Template::create($data);

        return redirect(route('dashboard.template.index'))->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template)
    {
        return view('dashboard.template.show', compact('template'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        return view('dashboard.template.edit', compact('template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Template $template)
    {

        $message = sprintf('Template %s successfully updated', $template->name);

        $template->update($request->all());

        return redirect(route('dashboard.template.show', $template->id))->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Template $template)
    {
        $message = sprintf('Template %s successfully deleted', $template->name);

        $template->delete();

        return redirect(route('dashboard.template.index'))->with('success', $message);
    }
}
