<?php

namespace App\Http\Controllers\Dashboard;

use App\Contact;
use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Message\GroupSendRequest;
use App\Http\Requests\Dashboard\Message\SendRequest;
use App\Jobs\SMS\SendJob;
use App\Message;
use App\Template;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        $messages = $user->messages()->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.message.index', compact('messages'));
    }

    public function show(Message $message)
    {
        return view('dashboard.message.show', compact('message'));
    }

    public function compose(Request $request, Contact $destination = null, $template = null)
    {
        $user = $request->user();

        $contacts = $user->contacts;
        $templates = $user->templates;

        if(isset($destination))
        {
            $destination = $destination->phone;
        }

        return view('dashboard.message.compose.index', compact('contacts', 'templates', 'destination', 'template'));
    }

    public function contactCompose(Contact $contact, Request $request)
    {
        return $this->compose($request, $contact->phone);
    }

    public function templateCompose(Template $template, Request $request)
    {
        return $this->compose($request, null, $template->message);
    }

    public function groupCompose($group = null, Request $request)
    {
        $user = $request->user();

        $groups = $user->groups;
        $templates = $user->templates;

        if(isset($group))
        {
            $group = $user->contacts()->findOrFail($group);
        }

        return view('dashboard.message.compose.group', compact('group', 'groups', 'templates'));
    }

    public function send(SendRequest $request)
    {
        $message['destination'] = $request->destination;
        $message['text'] = $request->message;
        $message['user_id'] = $request->user()->id;
        $message['status'] = 'stored';

        $this->sendJob(Message::create($message), $request->user());

        $message = __('dashboard.message.accepted');

        return redirect(route('dashboard.message.index'))->with('success', $message);
    }

    public function groupSend(GroupSendRequest $request)
    {
        exit(var_dump($request->group));
        $group = Group::find($request->group);

        $message['text'] = $request->message;
        $message['user_id'] = $request->user()->id;
        $message['status'] = 'stored';

        foreach ($group->members as $destination) {
            $message['destination'] = $destination->phone;

            $this->sendJob(Message::create($message), $request->user());
        }

        $message = __('dashboard.message.accepted');

        return redirect(route('dashboard.message.index'))->with('success', $message);
    }

    public function destroy(Message $message)
    {
        $message->delete();

        return redirect(route('dashboard.message.index'))->with('success', 'Message successfully deleted');
    }

    protected function sendJob(Message $message, User $user)
    {
        $user->update(['quota' => $user->quota - 1]);

        return SendJob::dispatch($message)->onQueue('sms');
    }
}
