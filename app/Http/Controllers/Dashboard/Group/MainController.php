<?php

namespace App\Http\Controllers\Dashboard\Group;

use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Group\StoreRequest;
use App\Http\Requests\Dashboard\Group\UpdateRequest;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $groups = $request->user()->groups()->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.group.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = $request->user()->id;

        $group = Group::create($data);

        $message = sprintf('Group %s successfully created', $group->name);

        return redirect(route('dashboard.group.index'))->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return view('dashboard.group.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('dashboard.group.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Group $group)
    {
        $message = sprintf('Group %s successfully updated', $group->name);

        $group->update($request->all());

        return redirect(route('dashboard.group.show', $group->id))->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $message = sprintf('Group %s (%s) successfully deleted', $group->phone, $group->name);

        $group->delete();

        return redirect(route('dashboard.group.index'))->with('success', $message);
    }
}
