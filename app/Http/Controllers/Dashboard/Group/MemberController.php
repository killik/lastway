<?php

namespace App\Http\Controllers\Dashboard\Group;

use App\Contact;
use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Group\Member\StoreRequest;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group)
    {
        $members = $group->members()->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.group.member.index', compact('members', 'group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Group $group, Request $request)
    {
        $contacts = $request->user()->contacts()->whereDoesntHave('groups', function ($query) use ($group) {
            $query->whereGroupId($group->id);
        });

        $contacts = $contacts->orderBy('created_at', 'DESC')->paginate(4);

        return view('dashboard.group.member.add', compact('contacts', 'group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Group $group)
    {
        if (count($request->members) > 1) {
            $message = sprintf('Some contacts have been successfully added to group %s', $group->name);
        }

        else
        {
            $message = sprintf('Contact successfully added to group %s', $group->name);
        }

        $group->members()->attach($request->members);

        return redirect(route('dashboard.group.member.index', $group->id))->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group, Contact $contact)
    {
        $group->members()->detach($contact->id);

        $message = sprintf('Contact %s (%s) successfully removed from group %s', $contact->phone, $contact->name, $group->name);

        return redirect(route('dashboard.group.member.index', $group->id))->with('success', $message);
    }
}
