<?php

namespace App\Http\Controllers\API\Minimal\V1;

use App\Http\Controllers\Controller;
use App\Jobs\SMS\SendJob;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ResponseFactory $response)
    {
        $messages = $request->user()->messages()->where($request->input('filter'));

        $data = $messages->orderBy('created_at', 'DESC')->paginate($request->input('limit'));

        return $response->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [];

        $message['destination'] = $request->input('message.destination');
        $message['text'] = $request->input('message.body');
        $message['user_id'] = $request->user()->id;
        $message['status'] = 'stored';

        $this->sendJob(Message::create($message), $request->user());

        return __('dashboard.message.accepted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message, ResponseFactory $response)
    {
        return $response->json($message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message, ResponseFactory $response)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message, ResponseFactory $response)
    {
        return $response->json($message->delete());
    }

    protected function sendJob(Message $message, User $user)
    {
        $user->update(['quota' => $user->quota - 1]);

        return SendJob::dispatch($message)->onQueue('sms');
    }
}
