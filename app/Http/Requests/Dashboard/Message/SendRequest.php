<?php

namespace App\Http\Requests\Dashboard\Message;

use App\Rules\User\Quota;
use Illuminate\Foundation\Http\FormRequest;

class SendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'destination' => ['required', 'min:3', 'max:16', 'regex:/^[0-9\+][0-9]+$/', new Quota($this->user(), 1)],
            'message' => 'required|min:3|max:160'
        ];
    }
}
