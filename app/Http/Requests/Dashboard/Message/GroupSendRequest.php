<?php

namespace App\Http\Requests\Dashboard\Message;

use App\Group;
use App\Rules\Message\SendGroup\Exists;
use App\Rules\User\Quota;
use Illuminate\Foundation\Http\FormRequest;

class GroupSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $needed = 0;

        if ($group = $user->groups()->find($this->group)) {
            $needed = $group->members->count();
        }

        return [
            'group' => ['required', new Exists($user), new Quota($user, $needed)],
            'message' => 'required|min:3|max:160'
        ];
    }
}
