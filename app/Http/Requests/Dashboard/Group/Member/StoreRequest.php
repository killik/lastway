<?php

namespace App\Http\Requests\Dashboard\Group\Member;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('group'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'members' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'members.required' => 'You must select at least one contacts'
        ];
    }
}
