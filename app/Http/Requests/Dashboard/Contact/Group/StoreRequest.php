<?php

namespace App\Http\Requests\Dashboard\Contact\Group;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('contact'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'groups' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'groups.required' => 'You must select at least one groups'
        ];
    }
}
