<?php

namespace App\Http\Requests\Dashboard\Contact;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('contact'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(StoreRequest $reques)
    {
        return [
            'name' => 'required|min:3',
            'phone' => 'required|min:3|max:16|regex:/^[0-9\+][0-9]+$/',
            'address' => 'required|min:3'
        ];
    }
}
