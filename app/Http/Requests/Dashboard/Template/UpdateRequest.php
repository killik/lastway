<?php

namespace App\Http\Requests\Dashboard\Template;

use App\Rules\Template\Update\Name;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('template'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:24', new Name\Unique($this->user(), $this->route('template'))],
            'message' => 'required|min:3|max:160'
        ];
    }
}
