<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'quota',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function makeApiToken()
    {
        $this->api_token = Str::random(100);

        if($this->where('api_token', $this->api_token)->exists())
        {
            call_user_func([$this, __FUNCTION__]);
        }
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->makeApiToken();
        });
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'user_id', 'id');
    }

    public function groups()
    {
        return $this->hasMany(Group::class, 'user_id', 'id');
    }

    public function templates()
    {
        return $this->hasMany(Template::class, 'user_id', 'id');
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'user_id', 'id');
    }
}
