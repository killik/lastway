<?php

namespace App;

use App\Traits\Model\Relations\Owner;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use Owner;

    protected $fillable = ['name', 'user_id'];

    public function members()
    {
        return $this->belongsToMany(Contact::class);
    }
}
