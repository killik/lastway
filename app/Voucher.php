<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Voucher extends Model
{
    protected $fillable = ['quota'];

    protected function makeToken()
    {
        $this->token = Str::random(20);

        if ($this->where('token', $this->token)->exists()) {
            call_user_func([$this, __FUNCTION__]);
        }
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->makeToken();
            $model->quota = intval($model->quota);
        });
    }
}
