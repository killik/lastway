<?php

namespace App;

use App\Traits\Model\Relations\Owner;
use Illuminate\Database\Eloquent\Model;
class Contact extends Model
{
    use Owner;

    protected $fillable = ['name', 'phone', 'address', 'user_id'];

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'user_id', 'id');
    }
}
