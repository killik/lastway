<?php namespace App\Lingu;

use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Model\SendMessageRequest;

class SMSGatewayMe
{
    protected $messageClient;
    protected $configuration;
    protected $apiClient;
    protected $deviceID;

    public function __construct(string $key, int $deviceID)
    {
        $this->setApiKey($key)->setDeviceID($deviceID);
    }

    public function sendMessage(string $destination, string $message)
    {
        $sendMessageRequest = new SendMessageRequest([
            'phoneNumber' => $destination,
            'message' => $message,
            'deviceId' => $this->getDeviceID()
          ]);

          return $this->getMessageApi()->sendMessages([$sendMessageRequest])[0];
    }

    public function broadcastMessage(array $destinations, string $message)
    {
        foreach($destinations as $destination)
        {
            $sendMessageRequests[] = new SendMessageRequest([
                'phoneNumber' => $destination,
                'message' => $message,
                'deviceId' => $this->getDeviceID()
              ]);
        }

        return $this->getMessageApi()->sendMessages($sendMessageRequests);
    }

    public function getMessage(int $id)
    {
        return $this->getMessageApi()->getMessage($id);
    }

    public function searchMessage(array $filter)
    {
        return $this->getMessageApi()->searchMessages($filter);
    }

    public function getDeviceID()
    {
        return $this->deviceID;
    }

    public function getMessageApi()
    {
        return $this->messageApi ?? $this->messageApi = new MessageApi($this->getApiClient());
    }

    public function getApiClient()
    {
        return $this->apiClient ?? $this->apiClient = new ApiClient($this->getConfiguration());
    }

    public function getConfiguration()
    {
        return $this->configuration ?? $this->configuration = Configuration::getDefaultConfiguration();
    }

    public function setApiKey(string $key)
    {
        $this->getConfiguration()->setApiKey('Authorization', $key);

        return $this;
    }

    public function setDeviceID(int $id)
    {
        $this->deviceID = $id;

        return $this;
    }
}
