<?php

namespace App\Rules\Template\Update\Name;

use App\Template;
use App\User;
use Illuminate\Contracts\Validation\Rule;

class Unique implements Rule
{
    protected $user;
    protected $template;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(User $user, Template $template)
    {
        $this->user = $user;
        $this->template = $template;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $template = $this->user->templates()->whereName($value);

        return !$template->exists() || $template->first()->id === $this->template->id;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique');
    }
}
