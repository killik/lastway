<?php

namespace App\Rules\Group\Update;

use App\Group;
use App\User;
use Illuminate\Contracts\Validation\Rule;

class Unique implements Rule
{
    protected $user;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(User $user, Group $group)
    {
        $this->user = $user;
        $this->group = $group;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $group = $this->user->groups()->whereName($value);

        return !$group->exists() || $group->first()->id === $this->group->id;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique');
    }
}
