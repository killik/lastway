<?php

namespace App\Rules\User;

use App\User;
use Illuminate\Contracts\Validation\Rule;

class Quota implements Rule
{
    protected $user;
    protected $needed;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(User $user, int $needed)
    {
        $this->user = $user;
        $this->needed = $needed;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->user->quota >= $this->needed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('dashboard.user.quota.limit');
    }
}
