<?php

namespace App\Providers;

use App\Lingu\SMSGatewayMe;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSMS();
    }

    protected function registerSMS()
    {
        $config = $this->app['config']->get('smsgatewayme', []);

        $this->app->bindIf('sms', function () use($config) {
            return new SMSGatewayMe(strval($config['key']), intval($config['deviceID']));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
