<?php

namespace App\Console\Commands\Voucher;

use App\Voucher;
use Illuminate\Console\Command;

class Index extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List Vouchers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['Token', 'Quota', 'Created at'];

        $vouchers = Voucher::all(['token', 'quota', 'created_at']);

        $this->table($headers, $vouchers);
    }
}
