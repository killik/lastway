<?php

namespace App\Console\Commands\Voucher;

use App\Voucher;
use Illuminate\Console\Command;

class Create extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'voucher:generate {quota=8}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new vouchers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $voucher = Voucher::create($this->arguments());

        $message = sprintf('<info>Voucher: <comment>%s</comment> successfully created, quota: %sx</info>', $voucher->token, $voucher->quota);

        $this->line($message);
    }
}
