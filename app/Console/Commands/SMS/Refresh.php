<?php

namespace App\Console\Commands\SMS;

use App\Jobs\SMS\UpdateJob;
use Illuminate\Console\Command;

class Refresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:status:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh sms sending status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        UpdateJob::dispatch()->onQueue('sms');

        $this->info(__('job.accepted'));
    }
}
