<?php

namespace App;

use App\Traits\Model\Relations\Owner;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use Owner;

    protected $fillable = [
        'destination',
        'text',
        'status',
        'sender',
        'user_id',
        'remote_id'
    ];

    public function destination()
    {
        return $this->hasOne(Contact::class, 'phone', 'destination');
    }

    public function sender()
    {
        return $this->hasOne(Contact::class, 'phone', 'sender');
    }
}
